<?php
return [
  'middleware'    =>    [
    think\admin\middleware\CrossDomain::class,
    think\admin\middleware\Auth::class,
  ],
];
